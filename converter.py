from ConverterGUI import *
import wx
import os
import re
import string

__author__ = 'Pato'
DEBUG = True


domains = "org,dj,com,net,info,biz"
replace_dict = {'&amp;': '&',
                '-': ' - ',
                '  ': ' ',
                '_': ' ',
                '--': '-',
                '  -   -  ': ''
                }


def delete_end(file):
    file = unicode(file)
    pos_end = file.find(')')
    if pos_end > False:
        return file[0:pos_end + 1]
    return file


def replace(file):
    file = unicode(file)
    for rep, with_ in replace_dict.iteritems():
        file = file.replace(rep, with_)
    return file


def delete_hypen(file):
    file = re.sub('^\w+-', ' -', file)
    file = re.sub('-+^\w', '- ', file)
    return file


def delete_keys(file):
    return re.sub('([1-9]+1-2|[0-9]+[A-B])', '', file)


def delete_bpms(file):
    return re.sub('([1-2]+1-9|[0-9]+[0-9])', '', file)


def replace_dupl_ws(file):
    return unicode(file).replace('\s\s+/g', ' ')


def converter(file):
    file = os.path.splitext(os.path.basename(file))[0]
    file = delete_keys(file)
    file = delete_bpms(file)
    file = delete_end(file)
    file = delete_hypen(file)
    file = replace(file)
    return file.title()


class DroppedFiles(wx.FileDropTarget):
    def __init__(self, frame, MAX):
        wx.FileDropTarget.__init__(self)
        self.frame = frame
        self.max = MAX

    def OnDropFiles(self, x, y, filenames):
        MAX = 100
        if len(filenames) < self.max:
            if len(filenames) and os.path.splitext(os.path.basename(filenames[0]))[1] == '.txt':
                txt_file = open(filenames[0], 'r')
                while True:
                    line = txt_file.readline()
                    if not line:
                        break
                    self.frame.text_filenames_converted.AppendText(converter(line) + '\n')
            else:
                print("%d file(s) dropped at (%d,%d):\n" % (len(filenames), x, y))
                for file in filenames:
                    self.frame.text_filenames_converted.AppendText(converter(file) + '\n')
        else:
            wx.MessageBox(u"Maximum " + unicode(MAX) + " files can be dropped.", "Information", wx.ICON_INFORMATION)


class ConverterApp(wx.App):
    def __init__(self):
        if DEBUG is True:
            super(ConverterApp, self).__init__()
        else:
            super(ConverterApp, self).__init__(redirect=True, filename="log.txt")
        self.mainframe = ConverterFrame(None)
        self.mainframe.Show()
        MAX = 100
        self.mainframe.label_drop_files.SetToolTipString(u"Drop one text file or max. %d audio files..." % MAX)
        dt = DroppedFiles(self.mainframe, MAX)
        self.mainframe.label_drop_files.SetDropTarget(dt)

        self.mainframe.btn_copy_to_clipboard.Bind(wx.EVT_BUTTON, self.CopyToClipboard)

    def CopyToClipboard(self, Event):
        if len(self.mainframe.text_filenames_converted.GetValue()) > 0:
            data = wx.TextDataObject()
            data.SetText(self.mainframe.text_filenames_converted.GetValue())
            if wx.TheClipboard.Open():
                wx.TheClipboard.SetData(data)
                wx.TheClipboard.Close()
            else:
                wx.MessageBox("Unable to open the clipboard.", "Error", wx.ICON_ERROR)


if __name__ == "__main__":
    app = ConverterApp()
    app.MainLoop()
