# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc


###########################################################################
## Class ConverterFrame
###########################################################################

class ConverterFrame(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"Pato's Filename Converter v1.0",
                          pos=wx.DefaultPosition, size=wx.Size(-1, -1), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHintsSz(wx.Size(450, -1), wx.DefaultSize)

        mainSizer = wx.BoxSizer(wx.VERTICAL)

        sizer_wrapper = wx.BoxSizer(wx.VERTICAL)

        sizer_wrapper.SetMinSize(wx.Size(-1, 200))
        self.label_drop_files = wx.StaticText(self, wx.ID_ANY, u"\nDROP FILES HERE", wx.Point(-1, -1), wx.Size(-1, 150),
                                              wx.ALIGN_CENTRE)
        self.label_drop_files.Wrap(-1)
        self.label_drop_files.SetFont(wx.Font(28, 74, 93, 92, False, "@Arial Unicode MS"))

        sizer_wrapper.Add(self.label_drop_files, 0, wx.ALL | wx.EXPAND, 5)

        self.text_filenames_converted = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                    wx.Size(-1, 300), wx.TE_DONTWRAP | wx.TE_MULTILINE)
        self.text_filenames_converted.SetFont(wx.Font(11, 74, 90, 90, False, "Arial"))

        sizer_wrapper.Add(self.text_filenames_converted, 1, wx.ALL | wx.EXPAND, 1)

        self.btn_copy_to_clipboard = wx.Button(self, wx.ID_ANY, u"COPY TO CLIPBOARD", wx.DefaultPosition,
                                               wx.DefaultSize, 0)
        self.btn_copy_to_clipboard.SetFont(wx.Font(12, 74, 90, 90, False, "Arial Unicode MS"))

        sizer_wrapper.Add(self.btn_copy_to_clipboard, 0, wx.ALL | wx.EXPAND, 0)

        mainSizer.Add(sizer_wrapper, 1, wx.EXPAND, 5)

        self.SetSizer(mainSizer)
        self.Layout()
        mainSizer.Fit(self)

        self.Centre(wx.BOTH)

    def __del__(self):
        pass
